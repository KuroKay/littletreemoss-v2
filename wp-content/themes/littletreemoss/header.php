<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package littletreemoss
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<!-- Bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header header">
		<div class="mobile-menu">
		<a href="/" class="logo-mobile">
				<?php 
            		$image = get_field('logo', 'option');;
            				if( !empty( $image ) ): ?>
                                        <img src="<?php echo esc_url($image['url']); ?>"
                                                alt="<?php echo esc_attr($image['alt']); ?>" />
                                <?php endif; ?>
				</a>
				<button class="burger">Menu</button>

							</div>
		<div class="container container-mobile">
		<button class="close"><i class="bi bi-x"></i></button>
			<div class="row">
				<div class="mode">
					<div id="lightmode" class="btn-mode"><i class="bi bi-brightness-high-fill icon"></i></i></div>
					<div id="darkmode" class="btn-mode"><i class="bi bi-moon icon"></i></div>
				</div>
				<a href="#" class="logo">
				<?php 
            		$image = get_field('logo', 'option');;
            				if( !empty( $image ) ): ?>
                                        <img src="<?php echo esc_url($image['url']); ?>"
                                                alt="<?php echo esc_attr($image['alt']); ?>" />
                                <?php endif; ?>
				</a>
				<nav id="site-navigation" class="main-navigation">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							)
						);
						?>
				</nav><!-- #site-navigation -->
				<div class="reseaux">
					<a href="https://www.instagram.com/littletreemoss/" target="_blank"><i class="bi bi-instagram icon"></i></a>
					<a href="https://discord.gg/UqAvMgvFSK" target="_blank"><i class="bi bi-discord"></i></a>
					<a href="mailto:h&#105;&#64;%6c%69&#116;&#116;l&#101;%74r%65&#101;%6do&#115;s%2e%63om"><i class="bi bi-envelope icon"></i></a>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
