<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package littletreemoss
 */

get_header();
?>
	<main id="primary" class="site-main single single-article">
        <div class="single-content">
            <?php 
                $image = get_field('article_image');
                if( !empty( $image ) ): ?>
                <div class="single-article_image">
                    <img src="<?php echo esc_url($image['url']); ?>"
                        alt="<?php echo esc_attr($image['alt']); ?>" />
                </div>
            <?php endif; ?>
            <div class="single-article_info">
                <span class="single-article_date"><?php the_time('d/m/Y'); ?></span>
                <h1 class="single-article_title"><?php the_field('article_title')?></h1>
                <div class="single-article_desc"><?php the_field('article_text')?></div>
            </div>
        </div>
	</main><!-- #main -->
<?php
get_footer();
