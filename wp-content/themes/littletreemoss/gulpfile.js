var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var sass = require('gulp-sass');

// Styles Task
gulp.task('styles', function() {
  return gulp.src('app/scss/style-custom.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest('dist/css'))
})

// Scripts Task
gulp.task('scripts', function() {
    return gulp.src('app/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

// Images Task
gulp.task('images', function() {
    return gulp.src('app/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});

// Watch Task
gulp.task('watch', function() {
  gulp.watch('app/scss/*.scss', gulp.series('styles')); 
  gulp.watch('app/js/*.js', gulp.series('scripts'));
  gulp.watch('app/img/*',gulp.series('images'));
});

// Default Task
gulp.task('default', gulp.parallel('styles', 'scripts', 'images', 'watch'));