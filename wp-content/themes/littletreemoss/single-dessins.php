<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package littletreemoss
 */

get_header();
?>

	<main id="primary" class="site-main single single-gallery">
		<?php 
            $image = get_field('draw_image');
            if( !empty( $image ) ): ?>
            <div class="single-gallery_image">
                <img src="<?php echo esc_url($image['url']); ?>"
                    alt="<?php echo esc_attr($image['alt']); ?>" />
            </div>
        <?php endif; ?>
		<div class="single-gallery_info">
			<h1 class="single-gallery_title"><?php the_field('draw_title')?></h1>
			<div class="single-gallery_desc"><?php the_field('draw_desc')?></div>
		</div>
	</main><!-- #main -->

<?php
get_footer();
