<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package littletreemoss
 */

get_header();
?>

<main id="primary" class="site-main single single-recipe">
        <div class="row row-intro">
            <?php 
                $image = get_field('recipe_image');
                if( !empty( $image ) ): ?>
                <div class="single-recipe_image">
                    <img src="<?php echo esc_url($image['url']); ?>"
                        alt="<?php echo esc_attr($image['alt']); ?>" />
                </div>
            <?php endif; ?>
            <div class="single-recipe_infos">
				<h1 class="single-recipe_title"><?php the_field('recipe_title')?></h1>
				<div class="single-recipe_desc"><?php the_field('recipe_desc')?></div>
				<div class="single-recipe_desc"><strong>Temps Total:</strong> <?php the_field('recipe_timetotal') ?></div>
				<div class="single-recipe_desc"><strong>Temps Préparation:</strong> <?php the_field('recipe_timeprepa')?></div>
				<div class="single-recipe_desc"><strong>Temps Cuisson:</strong> <?php the_field('recipe_timecuisine')?></div>
            </div>
		</div>
		
		<div class="row">
		<div class="ingredients">
					<h2>Ingrédients</h2>
				<p>Pour <?php the_field('recipe_nbperson')?> <?php the_field('recipe_person')?></p>
			<?php 

				// check for rows (parent repeater)
				if( have_rows('recipe_ingredient') ): ?>
					<div id="to-do-lists">
					<?php 

					// loop through rows (parent repeater)
					while( have_rows('recipe_ingredient') ): the_row(); ?>
						<div>
							<h2 class="recipe_ingredient-title"><?php the_sub_field('recipe_ingredient-title') ?></h2>
							<?php 

							// check for rows (sub repeater)
							if( have_rows('recipe_ingredient-section') ): ?>
								<ul>
								<?php 

								// loop through rows (sub repeater)
								while( have_rows('recipe_ingredient-section') ): the_row();

									// display each item as a list - with a class of completed ( if completed )
									?>
									<li>
										<span><?php the_sub_field('recipe_ingredient-quantity') ?></span>
										<span><?php the_sub_field('recipe_ingredient-unity') ?></span>
										<span><?php the_sub_field('recipe_ingredient-name') ?></span>
									</li>
								<?php endwhile; ?>
								</ul>
							<?php endif; //if( get_sub_field('items') ): ?>
						</div>	

					<?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
					</div>
				<?php endif; // if( get_field('to-do_lists') ): ?>
				</div>
		
			<div class="steps">
			<h2>Préparation</h2>
			<?php 
				// check for rows (parent repeater)
				if( have_rows('recipe_steps') ): ?>
					<div id="to-do-lists">
					<?php 

					// loop through rows (parent repeater)
					while( have_rows('recipe_steps') ): the_row(); ?>
						<div>
							<h2 class="recipe_steps-title"><?php the_sub_field('recipe_steps-title') ?></h2>
							<?php 

							// check for rows (sub repeater)
							if( have_rows('recipe_steps-section') ): ?>
								<ul>
								<?php 

								// loop through rows (sub repeater)
								while( have_rows('recipe_steps-section') ): the_row();

									// display each item as a list - with a class of completed ( if completed )
									?>
									<li>
										<p><?php the_sub_field('recipe_steps-text') ?></p>
									</li>
								<?php endwhile; ?>
								</ul>
							<?php endif; //if( get_sub_field('items') ): ?>
						</div>	

					<?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
					</div>
				<?php endif; // if( get_field('to-do_lists') ): ?>
			</div>
		</div>
	</main><!-- #main -->

<?php
get_footer();
