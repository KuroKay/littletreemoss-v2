<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package littletreemoss
 */

?>
</div><!-- #page -->

<footer>
    <div>
        <a href="/mentions-legales">Mentions légales</a>
    </div>
    <div>littletreemoss ©2021 by karen tschanz</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
