<?php
/**
 * Template Name: Politique
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package littletreemoss
 */

get_header();
?>
<main id="politique" class="page politique">
    <h1>Quelques mots</h1>
    <h2>1. Images</h2>
    <p>Toutes images présentes sur le site littletreemoss.com sont soumises à un copyright. Toutes utilisations, plagiats sans autorisation sont interdites.
    <h2>2. Données</h2>
    <p>Aucune donnée cliente n'est utilisée sur ce site</p>
</main><!-- #main -->
<?php
get_footer();