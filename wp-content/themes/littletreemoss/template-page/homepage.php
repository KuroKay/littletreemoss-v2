<?php
/**
 * Template Name: Homepage
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package littletreemoss
 */

get_header();
?>
<main id="primary" class="site-main home">
	<?php 
    	$image = get_field('home_image');
        if( !empty( $image ) ): ?>
	<div class="home_image">
		<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
	</div>
	<?php endif ?>
	<h1 class="home_title"><?php the_field('home_title')?></h1>
	<p class="home_desc"><?php the_field('home_desc')?></p>
	<section class="section-flex">
		<?php $args = array(
                'posts_per_page' => 1, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'dessins', /* your post type name */
                'post_status' => 'publish'
                );
                $query = new WP_Query($args);
                if ($query->have_posts()) :
                while ($query->have_posts()) : $query->the_post();
                 ?>
			<?php 
            $image = get_field('draw_image');
            if( !empty( $image ) ): ?>
			<div class="gallery-card_image">
				<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			</div>
			<?php endif; ?>
			<div class="gallery-card_body">
				<h2>Galerie</h2>
				<p>Si tu as envie de voir différents dessins alliant plusieurs techniques comme de la photographie ou de l'acrylique, c'est part ici ! Un monde en monochrome coloré s'ouvre à toi !</p>
				<a class="home_btn" href="/galerie"><span>Voir tous les dessins</span> <i class="bi bi-arrow-right icon"></i></a>
			</div>
		<?php
            endwhile;
            endif;
        ?>
	</section>


	<section>
		<h2>Les dernières recettes</h2>
		<p>Des recettes faisables sans gluten et lactose</p>
		<div class="recipes">
	<?php $args = array(
                'posts_per_page' => 3, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'recettes', /* your post type name */
                'post_status' => 'publish'
                );
                $query = new WP_Query($args);
                if ($query->have_posts()) :
                        while ($query->have_posts()) : $query->the_post();
                        $categories = get_the_category();            ?>
                <a class="element-item <?php echo $categories[0]->slug;?>" href="<?php the_permalink(); ?>"
                        data-category="<?php echo $categories[0]->slug;?>">
                        <div class="recipes-card recipes-card--<?php echo $categories[0]->slug;?>">
                                <?php 
            $image = get_field('recipe_image');
            if( !empty( $image ) ): ?>
                                <div class="recipes-card_image">
                                        <img src="<?php echo esc_url($image['url']); ?>"
                                                alt="<?php echo esc_attr($image['alt']); ?>" />
                                </div>
                                <?php endif; ?>
                                <div class="recipes-card_body">
                                        <h3 class="recipes-card_body-title"><?php the_field('recipe_title') ?></h3>
                                        <span><?php echo $categories[0]->name;?></span>
                                        <p class="recipes-card_body-desc"><?php the_field('recipe_desc') ?></p>
                                        <i class="bi bi-arrow-right icon"></i>
                                </div>
                        </div>
                </a>
                <?php
                        endwhile;
                endif;
				?>
			</div>
			<div class="center">
			<a class="home_btn" href="/recettes"><span>Toutes les recettes</span><i class="bi bi-arrow-right icon"></i></a>	
			</div>
		</section>
	<section class="section-flex reverse">
		<?php $args = array(
                'posts_per_page' => 1, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'articles', /* your post type name */
                'post_status' => 'publish'
                );
                $query = new WP_Query($args);
                if ($query->have_posts()) :
                while ($query->have_posts()) : $query->the_post();
                 ?>
			<?php 
            $image = get_field('article_image');
            if( !empty( $image ) ): ?>
	
			<?php endif; ?>
			<div class="discover-card_body">
                                <h2>Découverte de la semaine</h2>
                                <span></span>
				<h3><?php the_field('article_title') ?></h3>
                                <p><?php the_field('article_extrait') ?></p>
				<div>
				<a class="home_btn" href="<?php the_permalink(); ?>"><span>Lire</span><i class="bi bi-arrow-right icon"></i></a>
				<a class="home_btn" href="/decouvertes"><span>Toutes les découvertes</span><i class="bi bi-arrow-right icon"></i></a>	
			</div>
			</div>
			<div class="discover-card_image">
				<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			</div>
		<?php
            endwhile;
            endif;
        ?>
	</section>
</main><!-- #main -->
<?php
get_footer();