const btn = document.querySelector("#darkmode");
const btnlight = document.querySelector("#lightmode");

const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");

const currentTheme = localStorage.getItem("theme");
if (currentTheme == "light") {
  document.body.classList.toggle("light-theme");
} else if (currentTheme == "dark") {
  document.body.classList.toggle("dark-theme");
}

btn.addEventListener("click", function () {
  if (prefersDarkScheme.matches) {
    document.body.classList.toggle("dark-theme");
    var theme = document.body.classList.contains("dark-theme")
      ? "dark"
      : "light";
  } else {
    document.body.classList.toggle("light-theme");
    var theme = document.body.classList.contains("light-theme")
      ? "light"
      : "dark";
  }
  localStorage.setItem("theme", theme);
});

btnlight.addEventListener("click", function () {
  if (prefersDarkScheme.matches) {
    document.body.classList.toggle("dark-theme");
    var theme = document.body.classList.contains("dark-theme")
      ? "dark"
      : "light";
  } else {
    document.body.classList.toggle("light-theme");
    var theme = document.body.classList.contains("light-theme")
      ? "light"
      : "dark";
  }
  localStorage.setItem("theme", theme);
});

jQuery(document).ready(function( $ ) {
  $('.burger').on('click', function(){
    $('.container-mobile').toggleClass('open')
    $('.page').toggleClass('noscroll')
  })
  $('.close').on('click', function(){
    $('.container-mobile').toggleClass('open')
    $('.page').toggleClass('noscroll')
  })
})

